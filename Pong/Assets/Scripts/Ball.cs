﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Move ball
public class Ball : MonoBehaviour
{
    public float speed = 200;

    private Rigidbody2D rigidbody2d;

    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        ResetPosition();
    }

    public void ResetPosition()
    {
        rigidbody2d.position = Vector3.zero;
        stopMoving();
        AddRandomForce();
    }

    private void AddRandomForce()
    {
        float x = Random.value < 0.5f ? -1 : 1;
        float y = Random.value < 0.5f ? Random.Range(-1, -0.5f) : Random.Range(0.5f, 1);

        Vector2 force = new Vector2(x, y) * speed;
        rigidbody2d.AddForce(force);
    }

    public void AddForce(Vector2 force)
    {
        rigidbody2d.AddForce(force);
    }

    public void stopMoving()
    {
        rigidbody2d.velocity = Vector3.zero;
    }
}
