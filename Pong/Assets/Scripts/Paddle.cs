﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Paddle base
public class Paddle : MonoBehaviour
{
    public float speed = 50;

    protected Rigidbody2D rigidbody2D;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    internal void ResetPosition()
    {
        rigidbody2D.position = new Vector2(rigidbody2D.position.x, 0);
        rigidbody2D.velocity = Vector2.zero;
    }
}
