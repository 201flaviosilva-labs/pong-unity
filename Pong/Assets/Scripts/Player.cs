﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Move Player Paddle
public class Player : Paddle
{
    Vector2 direction;

    void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) direction = Vector2.up;
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) direction = Vector2.down;
        else direction = Vector2.zero;
    }

    private void FixedUpdate()
    {
        if (direction.sqrMagnitude != 0) rigidbody2D.AddForce(direction * speed);
    }
}
