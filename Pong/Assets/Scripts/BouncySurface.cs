﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Increase the Ball speed on colliding with the GameObject
public class BouncySurface : MonoBehaviour
{
    public float strength;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Ball ball = collision.gameObject.GetComponent<Ball>();

        if (ball != null)
        {
            Vector2 force = -collision.GetContact(0).normal * strength;
            ball.AddForce(force);
        }
    }
}
