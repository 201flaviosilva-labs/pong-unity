﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Main manager with the Player and Computer score
// Restart the GameObjects when some player scores
// Update UI scores Texts
// Explode ball partcle system on score
public class GameManager : MonoBehaviour
{
    public Ball ball;
    public ParticleSystem partSystem;

    public Paddle playerPadle;
    public Paddle computerPadle;

    public Text playerScoreText;
    public Text computerScoreText;

    private int playerScore = 0;
    private int computerScore = 0;

    public void AddPlayerScore()
    {
        playerScore++;
        StartCoroutine(ResetGameCorroutine());
        playerScoreText.text = playerScore.ToString();
    }

    public void AddComputerScore()
    {
        computerScore++;
        StartCoroutine(ResetGameCorroutine());
        computerScoreText.text = computerScore.ToString();
    }

    private IEnumerator ResetGameCorroutine()
    {
        ball.stopMoving();
        partSystem.Play();

        yield return new WaitForSeconds(1);

        ResetGame();
    }

    private void ResetGame()
    {
        playerPadle.ResetPosition();
        computerPadle.ResetPosition();
        ball.ResetPosition();

        partSystem.Pause();
        partSystem.Clear();
    }
}
