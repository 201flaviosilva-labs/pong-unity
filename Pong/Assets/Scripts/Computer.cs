﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Move computer Paddle
public class Computer : Paddle
{
    public Rigidbody2D ballRigidbody2D;

    private void FixedUpdate()
    {
        Vector2 force = new Vector2(0, 0);

        if (ballRigidbody2D.position.x > 0 && ballRigidbody2D.velocity.x > 0)
        {
            if (ballRigidbody2D.position.y > transform.position.y) force = Vector2.up * speed;
            else if (ballRigidbody2D.position.y < transform.position.y) force = Vector2.down * speed;
        }
        else
        {
            if (0 > transform.position.y) force = Vector2.up * speed;
            else if (0 < transform.position.y) force = Vector2.down * speed;
        }

        rigidbody2D.AddForce(force);
    }
}
